{-# LANGUAGE OverloadedStrings #-}

import Conduit
import Conduit.MediaWiki
import AutoWikiStream

import qualified Data.Text as T
import qualified Data.HashMap.Strict as M
import qualified Data.ByteString.Char8 as B8

import System.Clock
import System.IO (hPutStr, stderr)
import System.Environment (getArgs)

minimumOnC f = do
    e <- await
    case e of
        Nothing -> return Nothing
        Just x -> go x
    where go e = do
              may_x <- await
              case may_x of
                  Nothing -> return $! Just e
                  Just x  -> if f e < f x then go e else go x

isRedirect page = case pageRedirect page of {Nothing -> False; Just _ -> True}

-- count occurances of each namespace
countNamespaces :: Monad m => ConduitT Page o m (M.HashMap T.Text Int)
countNamespaces = foldMC (\m n -> return $! M.insertWith (+) (pageNamespace n) 1 m) M.empty

-- most stale page
oldestPage :: Monad m => ConduitT Page o m (Maybe T.Text)
oldestPage = filterC (not . isRedirect)
          .| filterC ((== "") . pageNamespace)
          .| filterC (not . ("isambiguation" `T.isInfixOf`) . revContent . pageLatest)
          .| fmap pageTitle <$> minimumOnC pageEditedAt


getUs = (\(TimeSpec s ns) -> s * 1000000 + ns `div` 1000) <$> getTime Monotonic
displayProgressC :: MonadIO m => ConduitT a a m ()
displayProgressC = liftIO getUs >>= go 0
    where go off lastUs = do
              e <- await
              case e of
                  Nothing -> printNum off >> liftIO (hPutStr stderr "\n")
                  Just x -> do
                      yield x
                      t <- liftIO getUs
                      if t > lastUs + 1000000
                      then printNum (off + 1) >> go (off + 1) t
                      else go (off + 1) lastUs

          printNum x = liftIO $ hPutStr stderr $ "\x1b[2K\r" ++ show x

main = do
    filename <- flip fmap getArgs $ \args -> case args of
        []    -> error "Please gib filename"
        f : _ -> f

    stuff <- runConduitRes
         $ debugMediawikiAutodetect filename
        .| displayProgressC
        .| filterC ((== "Portal") . pageNamespace)
        .| fst <$> getZipSink ((,) <$> ZipSink lengthC <*> ZipSink dumpToFile)

    print stuff
    where dumpToFile = mapC (\p -> B8.pack $ show (pageTitle p) ++ "\n") .| sinkFile "portals"
