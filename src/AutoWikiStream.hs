{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}

module AutoWikiStream (sourceMediawikiAutodetect, debugMediawikiAutodetect) where

import Conduit
import qualified Data.Conduit.Zstd as Zstd
import Conduit.MediaWiki

import System.IO
import qualified Data.ByteString as BS

autodetectStream :: (MonadIO m, MonadThrow m) => FilePath -> ConduitT i BS.ByteString m ()
autodetectStream f = do
    h <- liftIO $ openFile f ReadMode
    some <- liftIO $ BS.hGet h 4
    liftIO $ hSeek h AbsoluteSeek 0

    let decomp = if | some == BS.pack [0x28, 0xb5, 0x2f, 0xfd] -> Zstd.decompress
                    | some == "<med"                           -> mapC id
                    | otherwise -> error $ "unknown magic number: " ++ show (BS.unpack some) ++ " (supported: raw, zstd)"

    sourceHandle h .| decomp

sourceMediawikiAutodetect f = autodetectStream f .| parseMediawikiC
debugMediawikiAutodetect  f = autodetectStream f .| debugMediawikiC
