{-# LANGUAGE OverloadedStrings #-}

module Conduit.MediaWiki (
    parseMediawikiC,
    debugMediawikiC,

    User(..),
    userName,
    Revision(..),
    Page(..),
    pageEditedAt
) where

import Conduit

import Data.XML.Types
import Text.XML
import Text.XML.Stream.Parse

import Control.Monad (join)
import Data.ByteString (ByteString)
import Data.IORef
import Data.Maybe
import Data.String
import Data.Text
import Data.Text.Read
import Data.Time
import qualified Data.HashMap.Strict as M
import qualified Data.Text.IO as T

import Conduit.MediaWiki.Types

namespaced :: IsString s => String -> s
namespaced s = fromString $ "{http://www.mediawiki.org/xml/export-0.10/}" <> s

notName :: Name -> NameMatcher Name
notName n = matching (/= n)

readIntMaybe :: Text -> Maybe Int
readIntMaybe t = case signed decimal t of
        Left _       -> Nothing
        Right (i, _) -> Just i

parseNamespaces :: MonadThrow m => ConduitT Event a m (Maybe [(Text, Text)])
parseNamespaces = do
    many_ $ ignoreTree (notName $ namespaced "namespaces") ignoreAttrs
    tagIgnoreAttrs (namespaced "namespaces") $ do
        many $ tag' (namespaced "namespace") (requireAttr "key" <* ignoreAttrs) $ \k -> do
            (,) <$> return k <*> content

parseContributor :: MonadThrow m => ConduitT Event a m (Maybe User)
parseContributor = parseIpUser `orE` parseRegisteredUser
    where parseIpUser = fmap IpUser <$> tagIgnoreAttrs (namespaced "ip") content
          parseRegisteredUser = do
              name <- tagIgnoreAttrs (namespaced "username") content
              mayId <- tagIgnoreAttrs (namespaced "id") content
              return $! RegisteredUser <$> name <*> (mayId >>= readIntMaybe)

-- TODO this could probably be generalized to also work with wikipedia dumps that contain
-- multiple revisions
parseRevision :: MonadThrow m => ConduitT Event a m (Maybe Revision)
parseRevision = tagIgnoreAttrs (namespaced "revision") $ do
    revId <- force "Revision ID missing" $ (>>= readIntMaybe) <$> tagIgnoreAttrs (namespaced "id") content
    parId <- (>>= readIntMaybe) <$> tagIgnoreAttrs (namespaced "parentid") content

    time_text <- force "Time missing" $ tagIgnoreAttrs (namespaced "timestamp") content
    let maybeTime = parseTimeM False defaultTimeLocale "%Y-%m-%dT%H:%M:%SZ" $ unpack time_text
    time <- force "Time didn't parse" $! return $! maybeTime

    user <- force "User missing" $ tag' (namespaced "contributor") (attr "deleted") $ \del ->
        case del of
            Just _ -> return DeletedUser
            Nothing -> force "Contributor fields missing" parseContributor
    ignoreAnyTreeContent -- <minor/>; maybe more options exist? havent seen any
    comment <- fromMaybe "" <$> tagIgnoreAttrs (namespaced "comment") content

    ignoreTree (namespaced "model") ignoreAttrs
    ignoreTree (namespaced "format") ignoreAttrs

    text <- force "Text missing" $ tagIgnoreAttrs (namespaced "text") content

    many_ $ ignoreTree (matching $ const True) ignoreAttrs -- ignore leftover trees; e.g. hash

    return $! Revision revId parId time user comment text

parsePage :: MonadThrow m => M.HashMap Text Text -> ConduitT Event a m (Maybe Page)
parsePage namespaces = tagIgnoreAttrs (namespaced "page") $ do
    title <- force "Title missing" $ tagIgnoreAttrs (namespaced "title") content

    ns_text <- force "Namespace missing" $ tagIgnoreAttrs (namespaced "ns") content
    ns <- force "Unknown namespace" $! return $! M.lookup ns_text namespaces

    artId <- force "Article ID missing" $ (>>= readIntMaybe) <$> tagIgnoreAttrs (namespaced "id") content
    mayRedir <- tag' (namespaced "redirect") (requireAttr "title") return
    revis <- force "Revision didn't parse" parseRevision

    return $! Page title ns artId mayRedir revis []

parseWiki :: MonadThrow m => ConduitT Event Page m (Maybe ())
parseWiki = do 
    tagIgnoreAttrs (namespaced "mediawiki") $ do
        names <- force "" $ tagIgnoreAttrs (namespaced "siteinfo") $ do
            M.fromList <$> force "Namespaces didn't parse" parseNamespaces
        manyYield $ parsePage names

parseMediawikiC :: (MonadThrow m) => ConduitT ByteString Page m ()
parseMediawikiC = parseBytes def .| force "Wiki didn't parse" parseWiki

-- prints the last page before an parsing error occured
debugMediawikiC :: (MonadResource m, MonadThrow m) => ConduitT ByteString Page m ()
debugMediawikiC = do
    bracketP (newIORef Nothing) printErrPage $ \ref -> parseMediawikiC
        .| (iterMC (liftIO . writeIORef ref . Just . pageTitle)
            >> liftIO (writeIORef ref Nothing))
    where printErrPage ref = readIORef ref >>= mapM_ (T.putStrLn . ("Crash on the page after: " <>))
