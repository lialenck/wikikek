module Conduit.MediaWiki.Types where

import Data.Time (UTCTime)
import Data.Text (Text, pack)
import Data.Hashable (Hashable, hash, hashWithSalt)
import Data.Bits (xor)

data User = RegisteredUser Text Int | IpUser Text | DeletedUser deriving (Eq, Show)

userName :: User -> Text
userName (RegisteredUser n _) = n
userName (IpUser ip) = ip
userName (DeletedUser) = pack "(deleted)"

data Revision = Revision {
    revId :: !Int,
    revParentId :: !(Maybe Int),
    revTimestamp :: !UTCTime,
    revUser :: !User,
    revComment :: !Text,
    revContent :: !Text
} deriving (Eq, Show)

data Page = Page {
    pageTitle :: !Text,
    pageNamespace :: !Text, -- should be a custom sum type
    pageId :: !Int,
    pageRedirect :: !(Maybe Text),
    pageLatest :: !Revision,
    pageOlder :: ![Revision]
} deriving (Eq, Show)
pageEditedAt = revTimestamp . pageLatest

-- Using users as keys might be useful, for analyzing things.
-- This doesn't hold as much for pages/revisions, as they're all somewhat unique anyway.
-- You have to be careful with `DeletedUser`s, as they're compared as equal, but
-- might've been different users originally.
instance Ord User where
    -- the order of patterns and comparisons in the first pattern are chosen to
    -- hopefully be good for performance.
    compare (RegisteredUser n1 id1) (RegisteredUser n2 id2) = compare id1 id2 <> compare n1 n2
    compare (RegisteredUser _ _) (IpUser _) = LT
    compare (IpUser _) (RegisteredUser _ _) = GT
    compare (IpUser t1) (IpUser t2) = compare t1 t2

    compare (DeletedUser) (DeletedUser) = EQ
    compare (DeletedUser) _ = LT
    compare _ (DeletedUser) = GT

instance Hashable User where
    hashWithSalt x (RegisteredUser userName userId) = x `hashWithSalt` userName `hashWithSalt` userId
    hashWithSalt x (IpUser ip) = hashWithSalt x ip
    hashWithSalt x (DeletedUser) = x
    
    hash (RegisteredUser userName userId) = hash userName `xor` hash userId
    hash (IpUser ip) = hash ip
    hash (DeletedUser) = hash (314159 :: Int) -- digits of pi. not sus
